import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { Reminder } from '../reminder';
import { taskListUrl } from '../environment';

library.add(faTimes);

import { without } from 'lodash';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Reminder List';
  theList: object[];

  constructor(private http: HttpClient) {

  }

  ngOnInit(): void {
    this.http.get<Object[]>(taskListUrl).subscribe(data => {
      this.theList = data;
    });
  }

  deleteRem(theRem: Reminder) {
    this.theList = without(this.theList, theRem); //delete
    this.http.delete( taskListUrl + theRem.id).subscribe((data:any) => {
      console.log(data);
    });
  }

  addRem(theRem: object){
    this.theList.unshift(theRem);
    this.http.post('http://localhost:3000/taskList', theRem).subscribe((data:any) => {
      console.log(data);
    });

  }

}
