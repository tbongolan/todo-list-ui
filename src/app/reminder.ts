export class Reminder {
    id: number;
    taskName: string;
    taskDesc: string;
    dueDate: string;
}
