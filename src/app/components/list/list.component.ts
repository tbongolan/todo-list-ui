import { Component, Output, Input, EventEmitter } from '@angular/core';
import { Reminder } from 'src/app/reminder';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html'
})
export class ListComponent {
  @Input() taskList;
  @Output() deleteEvt = new EventEmitter();

  handleDelete(theRem: Reminder) {
    this.deleteEvt.emit(theRem); //Hopefully this is where the delete fucntion between the database happens
  }


}
