import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  @Output() addEvt = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }


  uuidv1 = require('uuid/v1');


  handleAdd(formInfo: any) {
    const tempItem: Object = {
      id: this.uuidv1(),
      taskName: formInfo.taskName,
      dueDate: formInfo.dueDate,
      taskDesc: formInfo.taskDesc
    };
    this.addEvt.emit(tempItem);
  }

}
